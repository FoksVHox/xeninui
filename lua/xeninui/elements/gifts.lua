local PANEL = {}

XeninUI:CreateFont("XeninUI.Gift.Title", 19)
XeninUI:CreateFont("XeninUI.Gift.Subtitle", 15)

AccessorFunc(PANEL, "m_filter", "Filter")

function PANEL:Init()
  self.background.search = self.background:Add("XeninUI.TextEntry")

  self.background.scroll = self.background:Add("XeninUI.Scrollpanel.Wyvern")

  self.background.layout = self.background.scroll:Add("DListLayout")
  self.background.layout:Dock(FILL)
end

function PANEL:SetFilter(func)
  self.Filter = func
end

function PANEL:SetOnClick(func)
  self.OnClick = func
end

function PANEL:CreateRow(ply, id)
  local panel = self.background.layout:Add("DButton")
  panel:SetText("")
  panel:Dock(TOP)
  panel:SetTall(48)
  panel:DockMargin(0, 0, 8, 8)
  panel.background = XeninUI.Theme.Navbar
  panel.title = ply:Nick()
  panel.subtitle = ply:GetDonatorByRoleName()
  local donatorRank = ply:GetDonator()
  panel.cost = Store.Ranks[id].cost
  if (donatorRank and id > donatorRank and donatorRank != 0) then
    panel.cost = panel.cost - Store.Ranks[donatorRank].cost
  end
  if (Store.Ranks[id].sale) then
    panel.cost = panel.cost * Store.Ranks[id].sale
  end
  if (!panel.subtitle) then
    panel.subtitle = "Member"
  end
  panel.Paint = function(pnl, w, h)
    draw.RoundedBox(6, 0, 0, w, h, pnl.background)

    draw.SimpleText(pnl.title, "XeninUI.Gift.Title", h, 8, Color(230, 230, 230))
    draw.SimpleText(pnl.subtitle, "XeninUI.Gift.Subtitle", h, 23, Color(180, 180, 180))
    
    if (pnl.cost) then
      local canAfford = LocalPlayer():Credits():CanAfford(pnl.cost)
      local col = canAfford and Color(230, 230, 230) or XeninUI.Theme.Red

      draw.SimpleText(string.Comma(pnl.cost) .. " credits", "XeninUI.Gift.Title", w - 16, h / 2, col, TEXT_ALIGN_RIGHT, TEXT_ALIGN_CENTER)
    end
  end
  panel.OnCursorEntered = function(pnl)
    pnl:LerpColor("background", XeninUI.Theme.Primary)
  end
  panel.OnCursorExited = function(pnl)
    pnl:LerpColor("background", XeninUI.Theme.Navbar)
  end
  panel.DoClick = function(pnl)
    local canAfford = LocalPlayer():Credits():CanAfford(pnl.cost)
    if (!canAfford) then return end

    self:OnClick(ply)
  end

  panel.avatar = panel:Add("XeninUI.Avatar")
  panel.avatar:SetPlayer(ply, 64)
  panel.avatar:SetVertices(90)
  panel.avatar:SetMouseInputEnabled(false)

  panel.PerformLayout = function(pnl, w, h)
    pnl.avatar:SetPos(8, 8)
    pnl.avatar:SetSize(32, 32)
  end
end

function PANEL:Populate()
  self.background.layout:Clear()

  local tbl, id = self:Filter()

  for i, v in pairs(tbl) do
    self:CreateRow(v, id)
  end
end

function PANEL:PerformLayout(w, h)
  self.BaseClass.PerformLayout(self, w, h)

  local y = 40 + 16
  local x = 16

  self.background.search:SetPos(x, y)
  self.background.search:SetSize(250, 35)
  self.background.search:SetIcon(XeninUI.Materials.Search)
  self.background.search:SetPlaceholder("Search for a player name")

  self.background.scroll:SetSize(self.background:GetWide() - x * 2, self.background:GetTall() - y - self.background.search:GetTall() - 32)
  self.background.scroll:SetPos(x, y + self.background.search:GetTall() + 16)
end

vgui.Register("XeninUI.Gift", PANEL, "XeninUI.Popup")
