local UIBase
do
  local _class_0
  local _base_0 = {
    __name = "UIBase",
    setPos = function(self, x, y)
      if x == nil then x = self.x
      end
      if y == nil then y = self.y
      end
      self.x = x
      self.y = y

      return self
    end,
    setSize = function(self, w, h)
      if w == nil then w = self.w
      end
      if h == nil then h = self.h
      end
      self.w = w
      self.h = h

      return self
    end,
    setX = function(self, amt)
      self.x = amt

      return self
    end,
    getX = function(self)
      return self.x
    end,
    setY = function(self, amt)
      self.y = amt

      return self
    end,
    getY = function(self)
      return self.y
    end,
    setWidth = function(self, amt)
      self.w = amt

      return self
    end,
    getWidth = function(self)
      return self.w
    end,
    setHeight = function(self, amt)
      self.h = amt

      return self
    end,
    getHeight = function(self)
      return self.h
    end,
    setFill = function(self, color)
      self.fill = color

      return self
    end,
    setOutline = function(self, color)
      self.outline = color

      return self
    end,
    setRenderVector = function(self, vector)
      self.renderVector = vector

      return self
    end,
    setRenderAngle = function(self, angle)
      self.renderAngle = angle

      return self
    end,
    setUIScale = function(self, amt)
      self.uiscale = amt

      return self
    end,
    render = function(self, instance, type, scale, tbl)
      if tbl == nil then tbl = {}
      end
      if (type == "rect") then
        instance.p:Rect(self.x, self.y, self.w, self.h, self.fill, self.outline)
      end

      if (type == "button") then
        local clicked, pressing, hover = instance.p:Button(tbl.text, tbl.font, self.x, self.y, self.w, self.h, tbl.color)

        if (clicked and tbl.text != "___") then
          instance:onClick()
        end

        if (hover and !pressing and tbl.text != "___") then
          instance:onHover()
        end
      end

      if (type == "line") then
        instance.p:Line(self.x, self.y, self.w, self.h, tbl.color)
      end

      if (type == "text") then
        instance.p:Text(tbl.text, tbl.font, self.x, self.y, tbl.color or nil, tbl.halign or nil, tbl.valign or nil, tbl.scissor_rect or nil)
      end

      instance.p:SetUIScale(5)
      instance.p:Cursor()
      instance.p:Render(self.renderVector, self.renderAngle, 0.02)

      return self
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self)
      self.outline = Color(255, 255, 255)
      self.fill = Color(0, 0, 0, 0)
    end,
    __base = _base_0,
    __name = "UIBase"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  UIBase = _class_0
end

Icarus.UI.Base = UIBase

local RenderObject
do
  local _class_0
  local _base_0 = {
    __name = "RenderObject",
    rect = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "rect",
        data = {},
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setFill = function(tbl, value)
          tbl.data.fill = value

          return tbl
        end,
        setOutline = function(tbl, value)
          tbl.data.outline = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    navbar = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "navbar",
        data = {
          activePanel = 1,
          name = uniqueName
        },
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setFill = function(tbl, value)
          tbl.data.fill = value

          return tbl
        end,
        setOutline = function(tbl, value)
          tbl.data.outline = value

          return tbl
        end,
        setTabs = function(tbl, tabs)
          tbl.data.tabs = tabs

          return tbl
        end,
        setActiveTab = function(tbl, value)
          tbl.data.activeTab = value

          return tbl
        end,
        getActiveTab = function(tbl)
          return tbl.data.activeTab
        end,
        setActivePanel = function(tbl, value)
          tbl.data.activePanel = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    view = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "view",
        data = {
          pages = 0,
          activePage = 1,
          content = {},
          materials = {
            wood = wood
          }
        },
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setPages = function(tbl, value)
          tbl.data.pages = value

          return tbl
        end,
        setActivePage = function(tbl, value)
          tbl.data.activePage = value

          return tbl
        end,
        setContentPerPage = function(tbl, value)
          tbl.data.contentPerPage = value

          return tbl
        end,
        setContent = function(tbl, value)
          tbl.data.content = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    button = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "button",
        data = {
          onClick = function()
            iprint("Unregistered button press!")
          end,
          onHover = function()end
        },
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setText = function(tbl, value)
          tbl.data.text = value

          return tbl
        end,
        setFont = function(tbl, value)
          tbl.data.font = value

          return tbl
        end,
        setColor = function(tbl, value)
          tbl.data.color = value

          return tbl
        end,
        setHover = function(tbl, value)
          tbl.data.hover = value

          return tbl
        end,
        onClick = function(tbl, value)
          tbl.data.onClick = value

          return tbl
        end,
        onHover = function(tbl, value)
          tbl.data.onHover = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    text = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "text",
        data = {
          halign = TEXT_ALIGN_LEFT,
          valign = TEXT_ALIGN_TOP
        },
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setText = function(tbl, value)
          tbl.data.text = value

          return tbl
        end,
        setFont = function(tbl, value)
          tbl.data.font = value

          return tbl
        end,
        setHAlign = function(tbl, value)
          tbl.data.halign = value

          return tbl
        end,
        setVAlign = function(tbl, value)
          tbl.data.valign = value

          return tbl
        end,
        setColor = function(tbl, value)
          tbl.data.color = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    slider = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "slider",
        data = {
          frac = 0
        },
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setColor = function(tbl, value)
          tbl.data.color = value

          return tbl
        end,
        setFrac = function(tbl, value)
          tbl.data.frac = value

          return tbl
        end,
        getFrac = function(tbl)
          return tbl.data.frac
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    material = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "material",
        data = {},
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setColor = function(tbl, value)
          tbl.data.color = value

          return tbl
        end,
        setMaterial = function(tbl, value)
          tbl.data.mat = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }
    end,
    line = function(self, uniqueName)
      if self.tbl[uniqueName] then
        return self.tbl[uniqueName]
      end

      self.tbl[uniqueName] = {
        ui = "line",
        data = {},
        setX = function(tbl, value)
          tbl.data.x = value

          return tbl
        end,
        setY = function(tbl, value)
          tbl.data.y = value

          return tbl
        end,
        setWidth = function(tbl, value)
          tbl.data.width = value

          return tbl
        end,
        setHeight = function(tbl, value)
          tbl.data.height = value

          return tbl
        end,
        setColor = function(tbl, value)
          tbl.data.color = value

          return tbl
        end,
        setVariable = function(tbl, name, value)
          tbl.data[name] = value

          return tbl
        end,
        getVariable = function(tbl, name)
          return tbl.data[name]
        end
      }

      return self.tbl[uniqueName]
    end,
    setVariable = function(self, name, value)
      self.data[name] = value

      return self
    end,
    getVariable = function(self, name)
      return self.data[name]
    end,
    setPos = function(self, pos)
      self.pos = pos

      return self
    end,
    getPos = function(self)
      return self.pos
    end,
    setAngle = function(self, ang)
      self.ang = ang

      return self
    end,
    getAngle = function(self)
      return self.ang
    end,
    setScale = function(self, scale)
      self.scale = scale

      return self
    end,
    getScale = function(self)
      return self.scale
    end,
    setUIScale = function(self, scale)
      self.uiscale = scale

      return self
    end,
    getUIScale = function(self)
      return self.uiscale
    end,
    renderRect = function(self, tbl)
      local data = tbl.data
      local x = data.x
      local y = data.y
      local w = data.width
      local h = data.height
      local fill = data.fill
      local outline = data.outline

      self.p:Rect(x, y, w, h, fill, outline)
    end,
    renderNavbar = function(self, tbl)
      local data = tbl.data

      for i, v in pairs(data.tabs) do
        local col = data.activeTab == i and XeninUI.Theme.Accent or color_white

        local y = 45 * (i - 1)

        local press = self.p:Button(v.name, "Icarus.UI.Button", tbl.data.x or 0, (tbl.data.y or 0) + y, 120, 40, col)

        if press then
          tbl:setActiveTab(i)
          tbl:setActivePanel(1)
        end

        if (data.activeTab == i) then
          if v.panels[data.activePanel] then
            v.panels[data.activePanel](v)
          else
            data.activeTab = 1
          end
        end
      end
    end,
    renderView = function(self, tbl)
      local ply = LocalPlayer()

      local data = tbl.data
      local x = data.x
      local y = data.y
      local w = data.width
      local h = data.height
      local pos = data.pos
      local angles = data.angles
      local content = data.content

      local green = XeninUI.Theme.Green
      local red = XeninUI.Theme.Red
      --self.p:Rect(x, y, w, 40, fill, outline)

      local activePage = math.Clamp(data.activePage, 0, data.pages)
      local offsetX = x

      if (data.activePage > data.pages and data.pages != 0) then
        tbl:setActivePage(1)
      end

      for i = 1, data.pages do
        local pageWidth = (w - 15 - (5 * (data.pages - 1))) / data.pages
        local pageX = offsetX + (pageWidth + 5) * (i - 1)

        local col = data.activePage == i and XeninUI.Theme.Accent
        local pageString = "Page " .. i
        local click = self.p:Button(pageString, "Icarus.UI.Page", 5 + pageX, y + 5, pageWidth, 30, col)

        if click then
          tbl:setActivePage(i)
        end

        if content[data.activePage] then
          local contentPerPage = data.contentPerPage
          local offsetY = y + 40

          local offset = offsetY

          for i, v in pairs(content[data.activePage]) do
            local h = ((data.height - offsetY - 5) / contentPerPage) - 5
            local add, _, hovering = self.p:Button("", "Icarus.UI.Button", x + 5, offset, w - 15, h)
            local descPos = x + 10 + 6

            --if ((Icarus and Icarus.craft).settings.showWeaponIconsOnUI and v.icon) then
            --  self.p:Mat(v.icon, descPos, offset + 10, h - 20, h - 20, Color(242, 242, 242))
            --  descPos = descPos + h - 20 + 10
            --end


            local canUse = true

            self.p:Text(v.name, "Icarus.UI.ListView.Name", descPos, offset + h / 2 - h / 5 + 1, canUse and Color(242, 240, 240) or red, TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)
            local levelColor = XeninUI.Theme.Green
            self.p:Text(v.desc or "No description", "Icarus.UI.ListView.Desc", descPos, offset + h / 2 + h / 5, Color(242, 240, 240, 100), TEXT_ALIGN_LEFT, TEXT_ALIGN_CENTER)

            if (add) then
              local parent = tbl:getVariable("parent")

              if (parent) then
                local renderObject = tbl:getVariable("renderObject")

                if (renderObject and renderObject.tbl) then
                  tbl:setVariable("noRender", true)

                  parent:setActivePanel(2)
                  parent:SetSelected(v.id)

                  for i, v in pairs(renderObject.tbl) do
                    local recipe = i:find("DrawRecipe.")
          
                    if (recipe) then
                      v:setVariable("noRender", nil)
                    end
                  end
                end
              end
            end

            offset = offset + h + 5
          end
        end
      end
    end,
    renderButton = function(self, tbl)
      local data = tbl.data
      local text = data.text
      local font = data.font
      local x = data.x
      local y = data.y
      local w = data.width
      local h = data.height
      local col = data.color
      local hover = data.hover

      local button, press, hover = self.p:Button(text, font, x, y, w, h, col, hover)

      if button then
        data.onClick(tbl)
      end

      if hover then
        data.onHover(tbl)
      end
    end,
    renderText = function(self, tbl)
      local data = tbl.data
      local text = data.text
      local font = data.font
      local x = data.x
      local y = data.y
      local color = data.color
      local halign = data.halign
      local valign = data.valign

      self.p:Text(text, font, x, y, color, halign, valign)
    end,
    renderSlider = function(self, tbl)
      local data = tbl.data
      local x = data.x
      local y = data.y
      local w = data.width
      local h = data.height
      local color = data.color
      local frac = data.frac

      frac = self.p:Slider(frac, x, y, w, h, color)

      tbl:setFrac(frac)
    end,
    renderMaterial = function(self, tbl)
      local data = tbl.data or {}
      local mat = data.mat
      local x = data.x or 0
      local y = data.y or 0
      local w = data.width or 0
      local h = data.height or 0
      local color = data.color or Color(255, 255, 255)

      if (mat == nil) then return end

      self.p:Mat(mat, x, y, w, h, color)
    end,
    renderLine = function(self, tbl)
      local data = tbl.data
      local x = data.x
      local y = data.y
      local w = data.width
      local h = data.height
      local color = data.color

      self.p:Line(x, y, w, h, color)
    end,
    findRender = function(self, tbl)
      if tbl.data.noRender then return end

      if (tbl.ui == "rect") then
        self:renderRect(tbl)
      elseif (tbl.ui == "navbar") then
        self:renderNavbar(tbl)
      elseif (tbl.ui == "view") then
        self:renderView(tbl)
      elseif (tbl.ui == "button") then
        self:renderButton(tbl)
      elseif (tbl.ui == "text") then
        self:renderText(tbl)
      elseif (tbl.ui == "slider") then
        self:renderSlider(tbl)
      elseif (tbl.ui == "material") then
        self:renderMaterial(tbl)
      elseif (tbl.ui == "line") then
        self:renderLine(tbl)
      end
    end,
    render = function(self)
      for i, v in SortedPairs(self.tbl) do
        self:findRender(v)
      end

      self.p:Cursor()
      self.p:SetUIScale(self.uiscale)
      self.p:Render(self.pos, self.ang, self.scale)
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, pos, ang, scale, uiscale)
      self.pos = pos
      self.ang = ang
      self.scale = scale
      self.uiscale = uiscale

      self.p = Icarus.libs.ui.tdui.Create()
      self.p:SetSkin("xenin")

      self.tbl = {}
      self.data = {}
    end,
    __base = _base_0,
    __name = "RenderObject"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  RenderObject = _class_0
end

Icarus.UI.RenderObject = RenderObject

local RenderObject
do
  local _class_0
  local _base_0 = {
    __name = "RenderObject",
    setPos = function(self, pos)
      self.pos = pos

      return self
    end,
    getPos = function(self)
      return self.pos
    end,
    setAngle = function(self, ang)
      self.ang = ang

      return self
    end,
    getAngle = function(self)
      return self.ang
    end,
    setScale = function(self, scale)
      self.scale = scale

      return self
    end,
    getScale = function(self)
      return self.scale
    end,
    setUIScale = function(self, scale)
      self.uiscale = scale

      return self
    end,
    getUIScale = function(self)
      return self.uiscale
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self, pos, ang, scale, uiscale)
      self.pos = pos
      self.ang = ang
      self.scale = scale
      self.uiscale = uiscale
    end,
    __base = _base_0,
    __name = "RenderObject"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  RenderObject = _class_0
end
local UI
do
  local _class_0
  local _base_0 = {
    __name = "UI",
    setRender = function(self, renderObject)
      self.render = renderObject

      return self
    end
  }
  _base_0.__index = _base_0
  _class_0 = setmetatable({
    __init = function(self) end,
    __base = _base_0,
    __name = "UI"
  }, {
    __index = _base_0,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  UI = _class_0
end

Icarus["3D"] = UI
