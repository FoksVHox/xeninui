local UIBase = Icarus.UI.Base

local Slider
do
  local _class_0
  local _parent_0 = UIBase
  local _base_0 = {
    __name = "Slider",
    __base = UIBase.__base,
    setColor = function(self, col)
      self.color = col

      return self
    end,
    setMax = function(self, max)
      self.max = max

      return self
    end,
    setMin = function(self, min)
      self.min = min

      return self
    end,
    getFrac = function(self)
      return self.frac
    end,
    setFrac = function(self, frac)
      self.frac = frac

      return self
    end,
    getAmount = function(self)
      return self.frac * self.max
    end,
    getRoundedAmount = function(self)
      return math.Round(self.frac * self.max)
    end,
    render = function(self, scale)
      if scale == nil then scale = 0.1
      end
      self.frac = self.p:Slider(self.frac, self.x, self.y, self.w, self.h, self.color)

      self.p:Render(self.renderVector, self.renderAngle, scale)
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self)
      Slider.__parent.__init(self)

      self.p = Icarus.libs.ui.tdui.Create()
      self.p:SetSkin("xenin")

      self.frac = 0.5

      self.max = 10
      self.min = 1
      self.color = Color(255, 255, 255)
    end,
    __base = _base_0,
    __name = "Slider",
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local parent = rawget(cls, "__parent")
        if parent then return parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  Slider = _class_0
end

Icarus.UI.Slider = Slider
