local UIBase = Icarus.UI.Base

local Navbar
do
  local _class_0
  local _parent_0 = UIBase
  local _base_0 = {
    __name = "Navbar",
    __base = UIBase.__base,
    setFont = function(self, font)
      self.font = font

      return self
    end,
    setNavbarVector = function(self, vector)
      self.navbarVector = vector

      return self
    end,
    setNavbarAngle = function(self, angle)
      self.navbarAngle = angle

      return self
    end,
    setPanelWidth = function(self, amt)
      self.panelWidth = amt

      return self
    end,
    setPanelHeight = function(self, amt)
      self.panelHeight = amt

      return self
    end,
    setPanelVector = function(self, vector)
      self.panelVector = vector

      return self
    end,
    setPanelAngle = function(self, angle)
      self.panelAngle = angle

      return self
    end,
    getPanelHeight = function(self)
      return self.panelHeight
    end,
    getPanelWidth = function(self)
      return self.panelWidth
    end,
    getActiveTab = function(self)
      return self.activeTab
    end,
    getActiveTabName = function(self)
      return self.tabs[self.activeTab].name
    end,
    addTab = function(self, name, renderPanel)
      local i = table.Count(self.tabs) + 1

      self.tabs[i] = {}
      self.tabs[i].name = name
      self.tabs[i].renderPanels = renderPanel
    end,
    setActivePanel = function(self, panelNumber)
      self.activePanel = panelNumber

      return self
    end,
    render = function(self, scale)
      if scale == nil then scale = 0.1
      end
      for i, v in ipairs(self.tabs) do
        self.tabs[i].button = self.tabs[i].button or Icarus.UI.Button()
        self.tabs[i].button:setX(0):setY(45 * (i - 1)):setWidth(120):setHeight(40):setRenderVector(self.navbarVector):setRenderAngle(self.navbarAngle):setText(self.tabs[i].name):setFont(self.font or "Icarus.UI.Button"):render(scale).onClick  = function(pnl)
          local activeTab = self.activeTab

          self.activeTabCache[activeTab] = {
            lastTab = self.activePanel
          }

          self.activeTab = i

          local newActiveTab = (self.activeTabCache[i] and self.activeTabCache[i].lastTab)

          if (newActiveTab != nil) then
            self.activePanel = newActiveTab
          else
            self.activePanel = 1
          end
        end

        if (self.activeTab == i) then
          self.tabs[i].button:setColor(XeninUI.Theme.Accent)
        else
          self.tabs[i].button:setColor(color_white)
        end

        if (self.activeTab == i) then
          self.tabs[i].panel = self.tabs[i].panel or Icarus.UI.Rect()
          self.tabs[i].panel:setX(0):setY(0):setFill(Color(0, 0, 0, 90)):setWidth(self.panelWidth):setHeight(self.panelHeight):setRenderVector(self.panelVector):setRenderAngle(self.panelAngle):render(scale)

          self.tabs[i].contents = self.tabs[i].contents or self.tabs[i].renderPanels[self.activePanel](self, scale)
        end
      end
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self, tabs)
      if tabs == nil then tabs = {}
      end
      Navbar.__parent.__init(self)

      self.tabs = {}
      self.activeTabCache = {}

      self.activeTab = 0
      self.activePanel = 1

      for i, v in pairs(tabs) do
        self:addTab(v.name, v.panels)
      end
    end,
    __base = _base_0,
    __name = "Navbar",
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local parent = rawget(cls, "__parent")
        if parent then return parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  Navbar = _class_0
end

Icarus.UI.Navbar = Navbar
