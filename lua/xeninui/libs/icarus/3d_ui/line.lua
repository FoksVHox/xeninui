local UIBase = Icarus.UI.Base

local Line
do
  local _class_0
  local _parent_0 = UIBase
  local _base_0 = {
    __name = "Line",
    __base = UIBase.__base,
    setColor = function(self, col)
      self.color = col

      return self
    end,
    render = function(self, scale)
      if scale == nil then scale = 0.1
      end
      Line.__parent.render(self, self, "line", scale, {
        color = self.color
      })
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self)
      Line.__parent.__init(self)

      self.p = Icarus.libs.ui.tdui.Create()
      self.p:SetSkin("xenin")
    end,
    __base = _base_0,
    __name = "Line",
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local parent = rawget(cls, "__parent")
        if parent then return parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  Line = _class_0
end

Icarus.UI.Line = Line
