local UIBase = Icarus.UI.Base

surface.CreateFont("Icarus.UI.Button", {
  font = "Montserrat",
  size = 16 * 5,
  antialias = true
})

local Button
do
  local _class_0
  local _parent_0 = UIBase
  local _base_0 = {
    __name = "Button",
    __base = UIBase.__base,
    setText = function(self, text)
      self.text = text

      return self
    end,
    getText = function(self)
      return self.text
    end,
    setFont = function(self, font)
      self.font = font

      return self
    end,
    setColor = function(self, col)
      self.color = col

      return self
    end,
    getFont = function(self)
      return self.font
    end,
    onHover = function(self)
      return self
    end,
    onClick = function(self)
      print("Clicked button with text " .. self.text)

      return self
    end,
    render = function(self, scale)
      if scale == nil then scale = 0.1
      end
      Button.__parent.render(self, self, "button", scale, {
        font = self.font,
        text = self.text,
        color = self.color
      })

      return self
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self)
      Button.__parent.__init(self)

      self.p = Icarus.libs.ui.tdui.Create()
      self.p:SetSkin("xenin")

      self.color = color_white
      self.font = "Icarus.UI.Button"
    end,
    __base = _base_0,
    __name = "Button",
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local parent = rawget(cls, "__parent")
        if parent then return parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  Button = _class_0
end

Icarus.UI.Button = Button
