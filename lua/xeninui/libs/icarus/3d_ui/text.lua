local UIBase = Icarus.UI.Base

local Text
do
  local _class_0
  local _parent_0 = UIBase
  local _base_0 = {
    __name = "Text",
    __base = UIBase.__base,
    setText = function(self, text)
      self.text = text

      return self
    end,
    setFont = function(self, font)
      self.font = font

      return self
    end,
    setColor = function(self, col)
      self.color = col

      return self
    end,
    setVAlign = function(self, align)
      self.valign = align

      return self
    end,
    setHAlign = function(self, align)
      self.halign = align

      return self
    end,
    render = function(self, scale)
      if scale == nil then scale = 0.1
      end
      Text.__parent.render(self, self, "text", scale, {
        font = self.font,
        text = self.text,
        color = self.color,
        valign = self.valign,
        halign = self.halign
      })
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self)
      Text.__parent.__init(self)

      self.p = Icarus.libs.ui.tdui.Create()
      self.p:SetSkin("xenin")

      self.text = "Hello"
      self.font = "Icarus.UI.Button"

      self.color = nil

      self.halign = TEXT_ALIGN_LEFT
    end,
    __base = _base_0,
    __name = "Text",
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local parent = rawget(cls, "__parent")
        if parent then return parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  Text = _class_0
end

Icarus.UI.Text = Text
