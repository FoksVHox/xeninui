local UIBase = Icarus.UI.Base

surface.CreateFont("Icarus.UI.Page", {
  font = "Montserrat",
  size = 20 * 5,
  antialias = true
})

surface.CreateFont("Icarus.UI.ListView.Name", {
  font = "Montserrat",
  size = 22 * 5,
  antialias = true
})

surface.CreateFont("Icarus.UI.ListView.Desc", {
  font = "Montserrat",
  size = 15 * 5,
  antialias = true
})

surface.CreateFont("Icarus.UI.ListView.Price", {
  font = "Montserrat",
  size = 14 * 5,
  antialias = true
})

local ListView
do
  local _class_0
  local _parent_0 = UIBase
  local _base_0 = {
    __name = "ListView",
    __base = UIBase.__base,
    setPanelWidth = function(self, width)
      self.panelWidth = width

      return self
    end,
    setPanelHeight = function(self, height)
      self.panelHeight = height

      return self
    end,
    setPages = function(self, pages)
      self.pages = pages

      return self
    end,
    clearContentsCache = function(self)
      self.cachePage = nil

      self.cachePage = {}
    end,
    setContents = function(self, page, content)
      self.cachePage[page] = self.cachePage[page] or {}
      self.content[page] = content

      return self
    end,
    setContentHeight = function(self, height)
      self.contentHeight = height

      return self
    end,
    onInfoClick = function(self, id, content)
      return self
    end,
    onActionClick = function(self, id, content)
      return self
    end,
    render = function(self, scale)
      if scale == nil then scale = 0.1
      end
      local pages = self.pages


      local pageString = "Page " .. self.activePage .. " of " .. pages
      surface.SetFont("Icarus.UI.Page")
      local tw, th = surface.GetTextSize(pageString)
      tw = tw / 5

      self.pageRect = Icarus.UI.Rect()
      self.pageRect:setX(5):setY(5):setWidth(tw + 10):setHeight(30):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()

      self.pageText = Icarus.UI.Text()
      self.pageText:setX(10):setY(5 + 30 / 2):setText(pageString):setFont("Icarus.UI.Page"):setVAlign(TEXT_ALIGN_CENTER):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()

      local offsetX = 5 + tw + 10

      for i = 1, pages do
        local pageWidth = (self.panelWidth - offsetX - 5 * pages) / pages
        local pageX = offsetX + (pageWidth + 5) * (i - 1)

        self.cache[i] = self.cache[i] or Icarus.UI.Button()
        self.cache[i]:setX(5 + pageX):setY(5):setWidth(pageWidth):setHeight(30):setText(i):setFont("Icarus.UI.Page"):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render().onClick = function(pnl)
          self.cache[self.activePage]:setColor(nil)

          self.activePage = i

          self.cache[i]:setColor(Icarus.libs.getActiveTheme().accent)
        end

        if (self.activePage == 0) then
          self.activePage = 1

          self.cache[1]:setColor(Icarus.libs.getActiveTheme().accent)
        end

        if (self.activePage == i and self.content[i] != nil) then
          local offset = 50

          local k = i

          for i, v in pairs(self.content[i]) do
            self.cachePage[i] = self.cachePage[i] or {}
            self.cachePage[i].rect = self.cachePage[i].rect or Icarus.UI.Rect()
            self.cachePage[i].rect:setX(10):setY(offset):setFill(Color(0, 0, 0, 90)):setWidth(self.panelWidth - 15):setHeight(self.contentHeight):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()

            self.cachePage[i].name = self.cachePage[i].name or Icarus.UI.Text()
            self.cachePage[i].name:setX(17):setY(offset + 5):setText(v.name):setColor(Color(242, 240, 240)):setFont("Icarus.UI.ListView.Name"):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()

            self.cachePage[i].desc = self.cachePage[i].desc or Icarus.UI.Text()
            self.cachePage[i].desc:setX(17):setY(offset + 25):setText(v.desc):setColor(Color(242, 240, 240, 100)):setFont("Icarus.UI.ListView.Desc"):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()

            self.cachePage[i].price = self.cachePage[i].price or Icarus.UI.Text()
            self.cachePage[i].price:setX(17):setY(offset + 42):setText(DarkRP.formatMoney(v.price)):setColor(Color(242, 240, 240, 100)):setFont("Icarus.UI.ListView.Price"):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()

            self.cachePage[i].info = self.cachePage[i].info or Icarus.UI.Button()
            self.cachePage[i].info:setX(self.panelWidth - 72):setY(offset + 5):setWidth(62):setHeight(52 / 2 - 2.5):setText("Details"):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render().onClick = function(pnl)
              self:onInfoClick{
                page = k,
                idOnPage = i,
                contents = self.content[k][i]
              }
            end

            self.cachePage[i].addToCart = self.cachePage[i].addToCart or Icarus.UI.Button()
            self.cachePage[i].addToCart:setX(self.panelWidth - 72):setY(offset + 5 + (52 / 2 + 2.5)):setWidth(62):setHeight(52 / 2 - 2.5):setText("Craft"):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render().onClick = function(pnl)
              self:onActionClick{
                page = k,
                idOnPage = i,
                contents = self.content[k][i]
              }
            end

            offset = offset + self.contentHeight + 5
          end
        end
      end

      self.cache._mSeperator = self.cache._mSeperator or Icarus.UI.Rect()
      self.cache._mSeperator:setX(0):setY(0):setFill(Color(0, 0, 0, 90)):setWidth(self.panelWidth + 5):setHeight(40):setRenderVector(self.renderVector):setRenderAngle(self.renderAngle):render()
      return self
    end
  }
  _base_0.__index = _base_0
  setmetatable(_base_0, _parent_0.__index)
  _class_0 = setmetatable({
    __init = function(self)
      ListView.__parent.__init(self)

      self.p = Icarus.libs.ui.tdui.Create()
      self.p:SetSkin("xenin")

      self.cache = {}
      self.cachePage = {}

      self.content = {}

      self.activePage = 0
    end,
    __base = _base_0,
    __name = "ListView",
    __parent = _parent_0
  }, {
    __index = function(cls, parent)
      local val = rawget(_base_0, parent)
      if val == nil then local parent = rawget(cls, "__parent")
        if parent then return parent[parent]
        end
      else
        return val
      end
    end,
    __call = function(cls, ...)
      local _self_0 = setmetatable({}, _base_0)
      cls.__init(_self_0, ...)
      return _self_0
    end
  })
  if _parent_0.__inherited then _parent_0.__inherited(_parent_0, _class_0)
  end
  ListView = _class_0
end

Icarus.UI.ListView = ListView
